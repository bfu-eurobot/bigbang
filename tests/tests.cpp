#include "bigbang/types/costmap.h"
#include "bigbang/nodes/kalmannode.h"
// Херня которую тестируем
#include <gtest/gtest.h>
#include "ros/ros.h"


TEST(Costmap, basicOperations)
{
    Costmap<float> map(CostmapMetadata(3, 3));
    map.fillFromVector(std::vector<float>{1,1,1,
                                          1,1.5,1,
                                          1,1,2.5});
    map+=map;
    EXPECT_FLOAT_EQ(map.at(1,1), 3);
    EXPECT_FLOAT_EQ(map.at(2,2), 5);
}

TEST(Kalman, basicOperations)
{
    KalmanFilter::CovT Cov1;
    KalmanFilter::CovT Cov2;
    auto result = (Cov1.inv() + Cov2.inv()).inv();
    EXPECT_FLOAT_EQ(1, 1);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    //ros::init(argc, argv, "tester");
    //ros::NodeHandle nh;
    return RUN_ALL_TESTS();
}