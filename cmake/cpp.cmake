find_package(Qt5 REQUIRED COMPONENTS Core)
find_package(Boost REQUIRED COMPONENTS system)
find_package(OpenCV REQUIRED)
add_compile_options(-std=c++11)
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall -Wno-sign-compare -O3 -fPIC -ffast-math")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -fPIC -Wno-sign-compare")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -pthread")
set(CMAKE_STATIC_LINKER_FLAGS "${CMAKE_STATIC_LINKER_FLAGS}")
set(CMAKE_AUTOMOC ON)
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${Qt5Core_INCLUDE_DIRS}
  ${OpenCV_INCLUDE_DIRS}
  ${bigbang_lidar_sdk_INCLUDE_DIRS}
  ${radapter_broker_INCLUDE_DIRS} 
) 

add_library(Types STATIC
  include/bigbang/types/coord.hpp
  include/bigbang/types/position.hpp
  include/bigbang/types/costmap.hpp
  include/bigbang/types/rosparams.cpp
  include/bigbang/types/serializer.hpp #needs to be added, so moc gets linked
)
target_link_libraries(Types
  ${catkin_LIBRARIES}
)
# Node code should be static, I guess
add_library(nodebase STATIC
  include/bigbang/nodes/nodebase.cpp
)
target_link_libraries(nodebase
  ${catkin_LIBRARIES}
  ${OpenCV_LIBRARIES}
  Qt5::Core
  Types
)
##########
set(NODES_LIBRARIES ${catkin_LIBRARIES}
                    nodebase
                    Qt5::Core)
add_executable(costmap_server 
  include/bigbang/nodes/costmapservernode.h
  src/costmapservernode.cpp
)
target_link_libraries(costmap_server ${NODES_LIBRARIES})

add_executable(monte_carlo
  include/bigbang/utils/measurement_utils.hpp
  include/bigbang/utils/particles.hpp
  include/bigbang/nodes/monte_carlo.h
  src/monte_carlo.cpp
)
target_link_libraries(monte_carlo ${NODES_LIBRARIES})

add_executable(global_planer 
  include/bigbang/nodes/globalplanernode.h 
  src/globalplanernode.cpp
)
target_link_libraries(global_planer ${NODES_LIBRARIES})

add_executable(bigbang_rplidar
  include/bigbang/utils/beacons_shape.hpp
  include/bigbang/utils/rplidar_utils.h
  include/bigbang/utils/rplidar_utils.cpp
  include/bigbang/nodes/rplidarnode.h
  src/rplidarnode.cpp
)

target_link_libraries(bigbang_rplidar ${NODES_LIBRARIES} bigbang_rplidar_sdk)

install(DIRECTORY launch nodes
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
  USE_SOURCE_PERMISSIONS
)

if (CATKIN_ENABLE_TESTING)
  catkin_add_gtest(bigbang_tests tests/tests.cpp)
  target_link_libraries(bigbang_tests ${catkin_LIBRARIES} Core Nodes Types radapter-broker)
endif()
