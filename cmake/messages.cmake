add_message_files(
    FILES
      ArduinoCommand.msg
      MotorInfo.msg
      MotorParams.msg
      ServoCreateUpdate.msg
      PinReader.msg
      PinReaderResponce.msg
      Move2d.msg
      Measure2d.msg
      Reconfigure.msg
      RawImu.msg
      ServoCommand.msg
      MapObject.msg
      PlanerStatus.msg
      LaserBeacon.msg
      LaserBeacons.msg
      MonteCarloState.msg
      ArduinoStatus.msg
      ArduinoLed.msg
  )

add_service_files(
  FILES
    ExecuteScript.srv
    DirectMove.srv
    DirectDrift.srv
)